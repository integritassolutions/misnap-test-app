/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState } from 'react';
import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  useColorScheme,
  View,
  Button,
  NativeModules
} from 'react-native';

const { MiSnapModule } = NativeModules;

import {
  Colors,
  DebugInstructions,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';


const App: () => Node = () => {
  const isDarkMode = useColorScheme() === 'dark';
  const [viewMode, setViewMode] = useState('initial');
  const [frontImage, setFrontImage] = useState('');
  const [PDF417Code, setPDF417Code] = useState('');
  const [selfieImage, setSelfieImage] = useState('');
  const [captureData, setCaptureData] = useState({});

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  const styles = StyleSheet.create({
    headerView: {
      backgroundColor: "#d0d0da",
      alignContent: 'center',
      borderBottomWidth: 1,
      borderBottomColor: '#909090',
      marginBottom: 20
    },
    headerText: {
      fontSize: 24,
      fontWeight: '600',
      color: '#000',
      marginVertical: 20,
      textAlign: 'center'
    },
    sectionTitle: {
      fontSize: 24,
      fontWeight: '600',
    },
    highlight: {
      fontWeight: '700',
    },
    dataDump: {
      marginVertical: 20
    },
    viewSection: {
      width: '80%',
      alignContent: 'center', 
      marginLeft: '10%',  
      marginVertical: 10
    }
  });
  
  const onPressCapturePassport = async () => {
      await MiSnapModule.capturePassport({}).then(response => {
        console.log('sample response:',  response);
        setFrontImage(response.encodedImage);
        setViewMode('selfie');
      })
      .catch(error => {
        console.log('error type:',error.code, ' error message:',  error.message);
        setViewMode('selfie');
      })
  };


  const onPressCaptureDriversLicenseFront = async () => {
    await MiSnapModule.captureDriversLicenseFront({}).then(response => {
      console.log('sample response:',  response);
      setFrontImage(response.encodedImage);
      setViewMode('DLBack');
    })
    .catch(error => {
      setViewMode('DLBack');
      console.log('error type:',error.code, ' error message:',  error.message);
    })
  };


  const onPressCaptureDriversLicenseBack = async () => {
    await MiSnapModule.captureDriversLicenseBack({}).then(response => {
      console.log('sample response:',  response);
      setPDF417Code(response.PDF417Code);
      setViewMode('selfie');
    })
    .catch(error => {
      setViewMode('selfie');
      console.log('error type:',error.code, ' error message:',  error.message);
    })
  };


  const onPressCaptureSelfie = async () => {
    await MiSnapModule.captureSelfie({"testKey": "testValue"}).then(response => {
      console.log('sample response:',  response);
      setSelfieImage(response.encodedImage);
      setViewMode('fetching');
      fetchMitekInformation();
    })
    .catch(error => {
      console.log('error type:',error.code, ' error message:',  error.message);
    })
  };

  const onPressRestart = () => {
    setViewMode('initial');
    setFrontImage('');
    setPDF417Code('');
    setSelfieImage('');
  }

  const fetchMitekInformation = () => {
    const data = {
      "googleApplicationToken": "UniqueGoogleApplicationToken",
      "images": {
        "idFront": frontImage,
        "idBack": PDF417Code,
        "selfie": selfieImage
      }
    };
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    fetch("https://stoplight.io/mocks/tailwind-sfcu/sfcu-google/22640480/updateApplicationImage", {
      method: "post",
      headers: myHeaders,
      body: JSON.stringify(data)
    })
      .then(response => response.json())
      .then(responseJson => {
        setCaptureData(responseJson);                  
        console.log(responseJson);
        setViewMode('response');
      })
      .catch(error => console.log('error', error));
  }

  const displayFormattedCapturedData = () => {
    return JSON.stringify(captureData);
  }


  return (
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={backgroundStyle}>
        <View style={styles.headerView}>
          <Text style={styles.headerText}>MISNAP TEST APP</Text>
        </View>  
        <View
          style={{
            backgroundColor: isDarkMode ? Colors.black : Colors.white,
          }}>
          { viewMode == 'initial' &&
            <View>
              <Text style={{width: '80%', marginLeft: '10%',  marginVertical: 10}}>Choose which form of capture you want to initiate:</Text>
              <View style={styles.viewSection}>
                <Button onPress={()=>{ onPressCaptureDriversLicenseFront() }}  title="Drivers License"/>
              </View>
              <View style={styles.viewSection}>
                <Button onPress={()=>{ onPressCapturePassport() }}  title="Passport"/>
              </View>
            </View>
          }
          { viewMode == 'DLBack' &&
          <View style={styles.viewSection}>
            <Text style={{width: '100%',  marginVertical: 10}}>Click on the button bellow to initiate the capture of the back of the driver's license:</Text>
            <Button onPress={()=>{ onPressCaptureDriversLicenseBack() }}  title="Driver licence (back)"/>
          </View>
          }
          { viewMode == 'selfie' &&
          <View style={styles.viewSection}>
            <Text style={{width: '100%',  marginVertical: 10}}>Now click on the button bellow to initiate Selfie Capture:</Text>
            <Button onPress={()=>{ onPressCaptureSelfie() }}  title="Selfie"/>
          </View>
          }
          { viewMode == 'fetching' &&
          <View style={styles.viewSection}>
            <Text>Fetching...</Text>
          </View>
          }
          { viewMode == 'response' &&
          <View style={styles.viewSection}>
            <Text style={styles.sectionTitle}>Capture Results</Text>
            <Text style={styles.dataDump}>{displayFormattedCapturedData()}</Text>
            <Button onPress={()=>{ onPressRestart() }}  title="Re-start Process"/>
          </View>
          }

        </View>
      </ScrollView>
  );
};




export default App;
