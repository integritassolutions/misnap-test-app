//
//  MiSnapManagerBridge.h
//  MiSnap
//
//  Created by Mac on 30/8/21.
//

#import <React/RCTBridgeModule.h>
#import "MiSnapVC.h"

@interface RCTMiSnapModule : NSObject <RCTBridgeModule, MiSnapVCDelegate>

@end

