//
//  MiSnapFacialCapture.swift
//  MiSnap
//
//  Created by Mac on 6/9/21.
//

import UIKit
import MiSnapFacialCapture

class MiSnapSwiftVC: UIViewController {

  var miSnapFacialCaptureVC: MiSnapFacialCaptureViewController? = nil
  let appDelegate = UIApplication.shared.delegate as! AppDelegate

  @objc var delegate : MiSnapVCDelegate?
  
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
  @objc func snapItButtonPressed(_ selectedJobType: String) {
    let parameters = MiSnapFacialCaptureParameters.init()!
    
    parameters.selectOnSmile = false
            
    parameters.cameraParameters.recordVideo = false
    parameters.cameraParameters.recordAudio = false

    miSnapFacialCaptureVC = MiSnapFacialCaptureViewController.init(with: parameters, delegate: self, review: .autoAndManual)
    guard let miSnapFacialCaptureVC = miSnapFacialCaptureVC else { fatalError("Could not initialize MiSnapFacialCaptureViewController") }
    miSnapFacialCaptureVC.modalPresentationStyle = .fullScreen
    miSnapFacialCaptureVC.modalTransitionStyle = .crossDissolve
    
    presentFacialCaptureVC(miSnapFacialCaptureVC)
  }

  func presentAlert(withTitle title: String, message: String) {
      DispatchQueue.main.async { [unowned self] in
          let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
          let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
          alert.addAction(ok)
          
          self.present(alert, animated: true, completion: nil)
      }
  }

  
  func presentFacialCaptureVC(_ vc: MiSnapFacialCaptureViewController) {
      // Set modalPresentationStyle to fullScreen explicitly to avoid undefined behavior on iOS 13 or newer
      vc.modalPresentationStyle = .fullScreen
      vc.modalTransitionStyle = .crossDissolve
      
      // It's recommended to check that there's at least 20 MB of free disc space available before starting a session when video recording is enabled
      let minDiskSpace = 20
      if vc.parameters.cameraParameters.recordVideo && !vc.hasMinDiskSpace(minDiskSpace) {
          presentAlert(withTitle: "Not Enough Space", message: "Please, delete old/unused files to have at least \(minDiskSpace) MB of free space")
          return
      }
      
      // Check camera permission before presentign a view controller to avoid undefined behavior
      vc.checkCameraPermission { [unowned self] (granted) in
          if !granted {
              var message = "Camera permission is required to capture your documents."
              if (vc.parameters.cameraParameters.recordVideo) {
                  message = "Camera permission is required to capture your documents and record videos of the entire process as required by a country regulation."
              }
              self.presentPermissionAlert(withTitle: "Camera Permission Denied", message: "\(message)\nPlease select \"Open Settings\" option and enable Camera permission")
              return
          }
          
          // Check microphone permission before presentign a view controller to avoid undefined behavior when video with audio recording is enabled
          if (vc.parameters.cameraParameters.recordVideo && vc.parameters.cameraParameters.recordAudio) {
              vc.checkMicrophonePermission { [unowned self] (granted) in
                  if !granted {
                      let message = "Microphone permission is required to record audio as required by a country regulation."
                      self.presentPermissionAlert(withTitle: "Microphone Permission Denied", message: "\(message)\nPlease select \"Open Settings\" option and enable Microphone permission")
                      return
                  }
                  
                  DispatchQueue.main.async { [unowned self] in
                      self.present(vc, animated: true, completion: nil)
                  }
              }
          } else {
              DispatchQueue.main.async { [unowned self] in
                  self.present(vc, animated: true, completion: nil)
              }
          }
      }
  }
  
  
  func presentPermissionAlert(withTitle title: String, message: String) {
      DispatchQueue.main.async { [unowned self] in
          let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Cancel", style: .default) { action in
          self.dismiss(animated: true) {
            self.delegate?.miSnapVCDelegateCancelledType("NO_PERMISSION", message: message)
          }
        }
          let openSettings = UIAlertAction(title: "Open Settings", style: .cancel) { (action) in
              if let url = URL(string: UIApplication.openSettingsURLString), UIApplication.shared.canOpenURL(url) {
                  UIApplication.shared.open(url, options: [:], completionHandler: nil)
              }
          }
          alert.addAction(cancel)
          alert.addAction(openSettings)
        DispatchQueue.main.async { [unowned self] in
          self.present(alert, animated: true, completion: nil)
        }
      }
  }
  
}

extension MiSnapSwiftVC: MiSnapFacialCaptureViewControllerDelegate {
    // MARK: MiSnapFacialCaptureViewControllerDelegate callbacks
    
    func miSnapFacialCaptureSuccess(_ results: MiSnapFacialCaptureResults) {
//        self.results = results
        print("MIBI Data:\n\(results.mibiDataString ?? "")")
      self.delegate?.miSnapVCDelegateFinishedReturningEncodedImage(results.encodedSelectedImage, results: (convertStringToDictionary(text: results.mibiDataString) ?? convertStringToDictionary(text: ""))!, andOriginalImage: results.selectedImage)
      appDelegate.goToReactNative()
    }
    
    func miSnapFacialCaptureCancelled(_ results: MiSnapFacialCaptureResults) {
        print("MIBI Data:\n\(results.mibiDataString ?? "")")
      self.delegate?.miSnapVCDelegateCancelledType("CANCELLED", message: "cancelled by user")
      appDelegate.goToReactNative()
    }
    
    func miSnapFacialCaptureDidFinishRecordingVideo(_ videoData: Data?) {
        // Check that videoData is not nil and handle it
    }
  
  func convertStringToDictionary(text: String) -> [String:AnyObject]? {
     if let data = text.data(using: .utf8) {
         do {
             let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
             return json
         } catch {
             print("Something went wrong")
         }
     }
     return nil
 }
}
