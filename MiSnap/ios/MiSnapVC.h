//
//  MiSnapVC.h
//  MiSnap
//
//  Created by Mac on 30/8/21.
//

#import <UIKit/UIKit.h>
#import <MiSnapSDK/MiSnapSDK.h>
#import "MiSnapSDKViewController.h"
#import "MiSnapBarcodeScannerViewController.h"
#import "MiSnapBarcodeScannerLightViewController.h"
#import "MiSnap-Swift.h"

NS_ASSUME_NONNULL_BEGIN

@protocol MiSnapVCDelegate <NSObject>
- (void) MiSnapVCDelegateFinishedReturningEncodedImage:(NSString *)encodedImage results:(NSDictionary *)results andOriginalImage:(UIImage *)image;
- (void) MiSnapVCDelegateCancelledType:(NSString *)type message:(NSString *)message;
@end


@interface MiSnapVC : UIViewController <MiSnapViewControllerDelegate, MiSnapBarcodeScannerDelegate, MiSnapBarcodeScannerLightDelegate>
- (void)snapItButtonPressed:(NSString*)selectedJobType;
@property (nonatomic, weak) id <MiSnapVCDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
