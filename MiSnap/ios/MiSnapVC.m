//
//  MiSnapVC.m
//  MiSnap
//
//  Created by Mac on 30/8/21.
//

#import "MiSnapVC.h"
#import "AppDelegate.h"

@interface MiSnapVC ()

@property (nonatomic, strong) MiSnapSDKViewController *miSnapController;
@property (nonatomic) NSString* selectedJobType;
@property (nonatomic, strong) UIImage *miSnapImage;
@property (nonatomic) NSDictionary *miSnapMibiData;
@property (nonatomic) BOOL showResults;
@property (nonatomic, strong) AppDelegate *appdelegate;

@end

@implementation MiSnapVC
@synthesize delegate; //synthesise  MiSnapVCDelegate delegate

- (instancetype)init
{
  self = [super init];
  if (self) {
    _appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
  }
  
  return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)snapItButtonPressed: (NSString*)selectedJobType
{

  self.miSnapController = nil;
  
  // Get the MiSnapViewController selected by the UIControl
  self.miSnapController = [[UIStoryboard storyboardWithName:@"MiSnapUX1" bundle:nil] instantiateViewControllerWithIdentifier:@"MiSnapSDKViewControllerUX1"];
  
  // Setup delegate, parameters, and transition style
  self.miSnapController.delegate = self;
  // Parameters will use auto capture
  self.selectedJobType = selectedJobType;

  NSDictionary *parameters = [self getMiSnapParameters:YES];
  
  [self.miSnapController setupMiSnapWithParams:parameters];
  self.miSnapController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
  
  // For iOS 13, UIModalPresentationFullScreen is not the default, so be explicit
  self.miSnapController.modalPresentationStyle = UIModalPresentationFullScreen;
  
  if ([self.selectedJobType isEqualToString:@"PDF417"])
  {
    
    dispatch_async(dispatch_get_main_queue(), ^{
      MiSnapBarcodeScannerViewController *vc = [MiSnapBarcodeScannerViewController instantiateFromStoryboard];
      vc.delegate = self;
      vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
      vc.modalPresentationStyle = UIModalPresentationFullScreen;
      [self presentViewController:vc animated:YES completion:nil];
    });
    
  
  }
  else if ([self.selectedJobType isEqualToString:@"PDF417 Light"])
  {
      
    dispatch_async(dispatch_get_main_queue(), ^{
      MiSnapBarcodeScannerLightViewController *vc = [MiSnapBarcodeScannerLightViewController instantiateFromStoryboard];
      vc.delegate = self;
      vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
      vc.modalPresentationStyle = UIModalPresentationFullScreen;
      [self presentViewController:vc animated:YES completion:nil];
    });
  }  else if ([self.selectedJobType isEqualToString:@"selfie"])
  {
      
    dispatch_async(dispatch_get_main_queue(), ^{
//      MiSnapFacialCaptureParameters *parameters = [MiSnapFacialCaptureParameters init];
//      parameters.selectOnSmile = false;
//              
//      parameters.cameraParameters.recordVideo = false;
//      parameters.cameraParameters.recordAudio = false;
//      MiSnapFacialCaptureViewController *vc = [MiSnapFacialCaptureViewController init];
      
    });
  } else
  {
      self.miSnapController.useBarcodeScannerLight = TRUE;
              
      [self presentMiSnapVC:self.miSnapController];
  }
}

- (void)presentMiSnapVC:(MiSnapSDKViewController *)vc
{
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    // Set modalPresentationStyle to Full Screen explicitly to avoid undefined behavior on iOS 13 and newer
    vc.modalPresentationStyle = UIModalPresentationFullScreen;
    
    NSInteger minDiskSpace = 20;
    if ([vc.captureParams[kMiSnapRecordVideo] boolValue] && ![vc hasMinDiskSpace:minDiskSpace])
    {
        [self presentAlertWithTitle:@"Not Enough Space" message:[NSString stringWithFormat:@"Please, clean up at least %d MB", (int)minDiskSpace]];
        return;
    }
    
    [vc checkCameraPermission:^(BOOL granted) {
        if (!granted)
        {
            //TODO: localize message
            NSString *message = @"Camera permission is required to capture your documents.";
            if ([vc.captureParams[kMiSnapRecordVideo] boolValue])
            {
                message = @"Camera permission is required to capture your documents and record videos of the entire process as required by a country regulation.";
            }
            
            [self presentPermissionAlertWithTitle:@"Camera Permission Denied" message:[message stringByAppendingString:@"\nPlease select \"Open Settings\" option and enable Camera permission"]];

            return;
        }
        
        if ([vc.captureParams[kMiSnapRecordVideo] boolValue] && [vc.captureParams[kMiSnapRecordAudio] boolValue])
        {
            [vc checkMicrophonePermission:^(BOOL granted) {
                if (!granted)
                {
                    //TODO: localize message
                    NSString *message = @"Microphone permission is required to record audio as required by a country regulation.";
                    
                    [self presentPermissionAlertWithTitle:@"Microphone Permission Denied" message:[message stringByAppendingString:@"\nPlease select \"Open Settings\" option and enable Microphone permission"]];
                    return;
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self presentViewController:vc animated:TRUE completion:nil];
                });
            }];
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self presentViewController:vc animated:TRUE completion:nil];
            });
        }
    }];
}

- (void)presentPermissionAlertWithTitle:(NSString *)title message:(NSString *)message
{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
          [self dismissViewControllerAnimated:TRUE completion:^{
            [self.delegate MiSnapVCDelegateCancelledType:@"NO_PERMISSION" message:message];
          }];
        }];
        UIAlertAction *openSettings = [UIAlertAction actionWithTitle:@"Open Settings" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            if ([UIApplication.sharedApplication canOpenURL:url])
            {
                [UIApplication.sharedApplication openURL:url options:@{} completionHandler:nil];
            }
        }];
        [alert addAction:cancel];
        [alert addAction:openSettings];
        
      dispatch_async(dispatch_get_main_queue(), ^{
          [self presentViewController:alert animated:TRUE completion:nil];
      });
    });
}

- (void)presentAlertWithTitle:(NSString *)title message:(NSString *)message
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:ok];
    
  dispatch_async(dispatch_get_main_queue(), ^{
      [self presentViewController:alert animated:TRUE completion:nil];
  });
}

#pragma mark - MiSnap Delegate methods

// New in MiSnap 4.1.1. These optional delgate methods provide greater flexibility for UX customization.
// This example shows how they can be used to control the timing of image analysis once the camera is
// started.  For example, a UX that displays an informational overlay can call pauseAnalysis to allow
// the overlay to complete before the document is captured.  Here, a delay can be set before
// resumeAnalysis is called. An event can also be implemented to call resumeAnalysis instead of a delay.
// By default, image analysis starts when the camera starts.
// By default, image analysis delays for 1 second after the first good frame is analyzed.

- (void)afterDismissMiSnap
{
    //self.miSnapController = nil;
   [self->_appdelegate goToReactNative];

}

// If it's important for your use case to know what document type was used at the moment of cancelling use undermentioned callback otherwise use miSnapCancelledWithResults:
// If a controller confrorms to both only miSnapCancelledWithResults:forDocumentType: will be called
// Called when the cancel button is pressed during capture
- (void)miSnapCancelledWithResults:(NSDictionary *)results forDocumentType:(NSString *)docType
{
    if ([docType isEqualToString:kMiSnapDocumentTypeDriverLicense])
    {
        // Handle Driver License cancel event
    }
    else if ([docType isEqualToString:kMiSnapDocumentTypeIdCardFront])
    {
        // Handle Id Card Front cancel event
    }
    else if ([docType isEqualToString:kMiSnapDocumentTypeIdCardBack])
    {
        // Handle Id Card Back cancel event
    }
    else if ([docType isEqualToString:kMiSnapDocumentTypePDF417])
    {
        // Handle PDF417 cancel event
    }
    // If needed, add other document types to handle cancel event for or delete unwanted
//      self.delegate?.miSnapVCDelegateCancelledType("CANCELLED", message: "cancelled by user")

    self.miSnapImage = nil;
    [self.delegate MiSnapVCDelegateCancelledType:@"CANCELLED" message:@"cancelled by user"];
    [self->_appdelegate goToReactNative];

}

// Called when the cancel button is pressed during capture
- (void)miSnapCancelledWithResults:(NSDictionary *)results
{
  self.miSnapImage = nil;
}

// If it's important for your use case to know what document type was used to capture a document use undermentioned callback otherwise use miSnapFinishedReturningEncodedImage:originalImage:andResults:
// If a controller confrorms to both only miSnapFinishedReturningEncodedImage:originalImage:andResults:forDocumentType: will be called
// Called when an image has been captured in either automatic or manual mode
- (void)miSnapFinishedReturningEncodedImage:(NSString *)encodedImage originalImage:(UIImage *)originalImage andResults:(NSDictionary *)results forDocumentType:(NSString *)docType
{
    if ([docType isEqualToString:kMiSnapDocumentTypeDriverLicense])
    {
        // Handle Driver License results
    }
    else if ([docType isEqualToString:kMiSnapDocumentTypeIdCardFront])
    {
        // Handle Id Card Front results
    }
    else if ([docType isEqualToString:kMiSnapDocumentTypeIdCardBack])
    {
        // Handle Id Card Back results
    }
    else if ([docType isEqualToString:kMiSnapDocumentTypePDF417])
    {
        // Handle PDF417 results
    }
    // If needed, add other document types to handle results for or delete unwanted
    
  [self processResultsEncodedImage:encodedImage results:results andOriginalImage:originalImage];
}

// Called when an image has been captured in either automatic or manual mode
- (void)miSnapFinishedReturningEncodedImage:(NSString *)encodedImage originalImage:(UIImage *)originalImage andResults:(NSDictionary *)results
{
    [self processResultsEncodedImage:encodedImage results:results andOriginalImage:originalImage];
}

- (void)didFinishRecordingVideo:(NSData *)videoData
{
    // Check that videoData is not nil and handle it
}

- (void)processResultsEncodedImage:(NSString *)encodedImage results:(NSDictionary *)results andOriginalImage:(UIImage *)image
{
    NSString* resultCode = results[kMiSnapResultCode];
    
    self.miSnapMibiData = results;
    
    if ([resultCode isEqualToString:kMiSnapBarcodeScannerResultSuccessPDF417])
    {
        NSString* pdf417Data = results[kMiSnapPDF417Data];
        char sepChar = [pdf417Data characterAtIndex:1];
        NSString* pdf417Hex = @"";
        
        for (int i = 0; i < pdf417Data.length; ++i)
        {
            char hexChar = [pdf417Data characterAtIndex:i];
            pdf417Hex = [pdf417Hex stringByAppendingFormat:@"%2.2x", hexChar];
        }
        
        NSString* message;
        
        if (('@' == [pdf417Data characterAtIndex:0]) && (0x0A == sepChar) && (0x0D == [pdf417Data characterAtIndex:3]))
        {
            NSString* sepString;
            
            if (0x0A == sepChar)
            {
                sepString = @"LF";
            }
            else
            {
                sepString = [@"0x" stringByAppendingFormat:@"%x", sepChar];
            }
            
            NSString* aamvaFmt               = [pdf417Data substringWithRange:NSMakeRange(4, 5)];
            NSString* issuingAuthorityCode = [pdf417Data substringWithRange:NSMakeRange(9, 6)];
            NSString* aamvaVer               = [pdf417Data substringWithRange:NSMakeRange(15, 2)];
            
            if ([aamvaVer isEqualToString:@"00"])
            {
                aamvaVer = @"00 - pre-standard";
            }
            else if ([aamvaVer isEqualToString:@"01"])
            {
                aamvaVer = @"01 - AAMVA DL/ID-2000";
            }
            else if ([aamvaVer isEqualToString:@"02"])
            {
                aamvaVer = @"02 - AAMVA Card Design Specification v1.0, dated 09-2003";
            }
            else if ([aamvaVer isEqualToString:@"03"])
            {
                aamvaVer = @"03 - AAMVA Card Design Specification v2.0, dated 03-2005";
            }
            else if ([aamvaVer isEqualToString:@"04"])
            {
                aamvaVer = @"04 - AAMVA Card Design Specification v1.0, dated 07-2009";
            }
            else if ([aamvaVer isEqualToString:@"05"])
            {
                aamvaVer = @"05 - AAMVA Card Design Specification v1.0, dated 07-2010";
            }
            else if ([aamvaVer isEqualToString:@"06"])
            {
                aamvaVer = @"06 - AAMVA Card Design Specification v1.0, dated 07-2011";
            }
            else if ([aamvaVer  isEqualToString:@"07"])
            {
                aamvaVer = @"07 - AAMVA Card Design Specification, current standard";
            }
            
            message = [NSString stringWithFormat:@"results = %@\nData =\n [ separator = %@"
                       " ;\n   aamvaFmt = %@"
                       " ;\n   Issuing Authority Code = %@"
                       " ;\n   aamvaVer = %@"
                       " ;\n\n   raw data:\n%@"
                       " \n\nraw hex = %@",
                       resultCode, sepString, aamvaFmt, issuingAuthorityCode, aamvaVer, pdf417Data, pdf417Hex];
        }
        else
        {
            message = [NSString stringWithFormat:@"results = %@"
                       "\npdf417 barcode does not conform to published AAMVA standards"
                       " ;\nraw data = %@"
                       " \n\nraw hex = %@",
                       resultCode, pdf417Data, pdf417Hex];
        }
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"2D Barcode Results" message:message preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:ok];

      dispatch_async(dispatch_get_main_queue(), ^{
          [self presentViewController:alert animated:TRUE completion:nil];
      });
        NSLog(@"2D Barcode Results %@", message);

      NSLog(@"PDF417 Data: %@", pdf417Data);
    }
    else
    {
        self.miSnapImage = [image copy];
    }
    
    [self afterDismissMiSnap];
    [self.delegate MiSnapVCDelegateFinishedReturningEncodedImage:encodedImage results:results andOriginalImage:image];

    self.showResults = TRUE;
}

//////////////////////////////////////////////////////////////////////
//     Setting MiSnap parameters for each specific document type    //
//////////////////////////////////////////////////////////////////////
- (NSDictionary *)getMiSnapParameters:(BOOL)useAutoCapture
{
    NSMutableDictionary* parameters = [NSMutableDictionary dictionaryWithDictionary:[MiSnapSDKViewController defaultParametersForACH]];
    
    // Must set specific server type and server version
    [parameters setObject:@"test" forKey:kMiSnapServerType];
    [parameters setObject:@"0.0" forKey:kMiSnapServerVersion];
    
    if([self.selectedJobType isEqualToString:@"CheckFront"])
    {
        parameters = [NSMutableDictionary dictionaryWithDictionary:[MiSnapSDKViewController defaultParametersForCheckFront]];
        [parameters setObject:@"Mobile Deposit Check Front" forKey:kMiSnapShortDescription];
        [parameters setObject:@"50" forKey:kMiSnapImageQuality];
    }
    else if([self.selectedJobType isEqualToString:@"CheckBack"])
    {
        parameters = [NSMutableDictionary dictionaryWithDictionary:[MiSnapSDKViewController defaultParametersForCheckBack]];
        [parameters setObject:@"Mobile Deposit Check Back" forKey:kMiSnapShortDescription];
        [parameters setObject:@"50" forKey:kMiSnapImageQuality];
    }
    else if([self.selectedJobType isEqualToString:kMiSnapDocumentTypePassport]) //@"PASSPORT"
    {
        parameters = [NSMutableDictionary dictionaryWithDictionary:[MiSnapSDKViewController defaultParametersForPassport]];
        [parameters setObject:@"Passport" forKey:kMiSnapShortDescription];
    }
    else if([self.selectedJobType isEqualToString:kMiSnapDocumentTypeDriverLicense]) //@"DRIVER_LICENSE"
    {
        parameters = [NSMutableDictionary dictionaryWithDictionary:[MiSnapSDKViewController defaultParametersForDriversLicense]];
        [parameters setObject:@"530" forKey:kMiSnapMinLandscapeHorizontalFill];
        [parameters setObject:@"License Front" forKey:kMiSnapShortDescription];
        [parameters setObject:@"0" forKey:kMiSnapTorchMode];
    }
    else if([self.selectedJobType isEqualToString:kMiSnapDocumentTypeIdCardFront]) //@"ID_CARD_FRONT"
    {
        parameters = [NSMutableDictionary dictionaryWithDictionary:[MiSnapSDKViewController defaultParametersForIdCardFront]];
        [parameters setObject:@"ID Card Front" forKey:kMiSnapShortDescription];
        [parameters setObject:@"0" forKey:kMiSnapTorchMode];
    }
    else if([self.selectedJobType isEqualToString:kMiSnapDocumentTypeIdCardBack]) //@"ID_CARD_FRONT"
    {
        parameters = [NSMutableDictionary dictionaryWithDictionary:[MiSnapSDKViewController defaultParametersForIdCardBack]];
        [parameters setObject:@"1" forKey:kMiSnapMaxCaptures]; // Shows how to set explicitly
        [parameters setObject:@"ID Card Back" forKey:kMiSnapShortDescription];
        [parameters setObject:@"0" forKey:kMiSnapTorchMode];
    }
    else if([self.selectedJobType isEqualToString:@"PDF417"])
    {
        [parameters setObject:kMiSnapDocumentTypePDF417 forKey:kMiSnapDocumentType];
        [parameters setObject:@"PDF417 Barcode Scanner" forKey:kMiSnapShortDescription];
    }
    else if([self.selectedJobType isEqualToString:@"PDF417 Light"])
    {
        [parameters setObject:kMiSnapDocumentTypePDF417 forKey:kMiSnapDocumentType];
        [parameters setObject:@"PDF417 Barcode Scanner Light" forKey:kMiSnapShortDescription];
    }
    else if([self.selectedJobType isEqualToString:@"LandscapeDoc"])
    {
        parameters = [NSMutableDictionary dictionaryWithDictionary:[MiSnapSDKViewController defaultParametersForLandscapeDocument]];
        [parameters setObject:@"Landscape Document" forKey:kMiSnapShortDescription];
    }
    
    if(useAutoCapture == NO)
    {
        [parameters setObject:@"1" forKey:kMiSnapCaptureMode];
    }
    
  return [parameters copy];
}


@end
