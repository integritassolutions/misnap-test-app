//
//  MiSnapManagerBridge.m
//  MiSnap
//
//  Created by Mac on 30/8/21.
//

// RCTCalendarModule.m
#import "RCTMiSnapModule.h"
#import <React/RCTLog.h>
#import "MiSnapVC.h"
#import "AppDelegate.h"
#import <MiSnap-Swift.h>

@interface RCTMiSnapModule ()
@property (nonatomic, strong) RCTPromiseResolveBlock callbackSucessBlock;
@property (nonatomic, strong) RCTPromiseRejectBlock callbackFailureBlock;

@end

@implementation RCTMiSnapModule 

// To export a module named RCTMiSnapModule
RCT_EXPORT_MODULE();

RCT_EXPORT_METHOD(createCalendarEvent:(NSString *)name location:(NSString *)location)
{
  RCTLogInfo(@"Pretending to create an event %@ at %@", name, location);
}

//RCT_EXPORT_METHOD(onPressedType:(NSString *)name)
//{
//
//  dispatch_async(dispatch_get_main_queue(), ^{
//    MiSnapVC *picker = [[MiSnapVC alloc] init];
//    [picker snapItButtonPressed:name];
//    UINavigationController* pickerNavigator = [[UINavigationController alloc] initWithRootViewController:picker];
//    pickerNavigator.modalPresentationStyle = UIModalPresentationFullScreen;
//    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//    [delegate goNativeViewController:pickerNavigator];
//  });
//
//  RCTLogInfo(@"Pretending to create an event %@", name);
//}

RCT_EXPORT_METHOD(capturePassport:(NSDictionary *)MTConfiguration resolve:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject){
  dispatch_async(dispatch_get_main_queue(), ^{
    MiSnapVC *picker = [[MiSnapVC alloc] init];
    [picker snapItButtonPressed:kMiSnapDocumentTypePassport];
    picker.delegate = self;
    self->_callbackSucessBlock = resolve;
    self->_callbackFailureBlock = reject;
    UINavigationController* pickerNavigator = [[UINavigationController alloc] initWithRootViewController:picker];
    pickerNavigator.modalPresentationStyle = UIModalPresentationFullScreen;
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [delegate goNativeViewController:pickerNavigator];
  });
}

RCT_EXPORT_METHOD(captureDriversLicenseFront:(NSDictionary *)MTConfiguration resolve:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject){
  dispatch_async(dispatch_get_main_queue(), ^{
    MiSnapVC *picker = [[MiSnapVC alloc] init];
    [picker snapItButtonPressed:kMiSnapDocumentTypeDriverLicense];
    picker.delegate = self;
    self->_callbackSucessBlock = resolve;
    self->_callbackFailureBlock = reject;
    UINavigationController* pickerNavigator = [[UINavigationController alloc] initWithRootViewController:picker];
    pickerNavigator.modalPresentationStyle = UIModalPresentationFullScreen;
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [delegate goNativeViewController:pickerNavigator];
  });
}

RCT_EXPORT_METHOD(captureDriversLicenseBack:(NSDictionary *)MTConfiguration resolve:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject){
  dispatch_async(dispatch_get_main_queue(), ^{
    MiSnapVC *picker = [[MiSnapVC alloc] init];
    [picker snapItButtonPressed:kMiSnapDocumentTypeIdCardBack];
    picker.delegate = self;
    self->_callbackSucessBlock = resolve;
    self->_callbackFailureBlock = reject;
    UINavigationController* pickerNavigator = [[UINavigationController alloc] initWithRootViewController:picker];
    pickerNavigator.modalPresentationStyle = UIModalPresentationFullScreen;
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [delegate goNativeViewController:pickerNavigator];
  });
}

RCT_EXPORT_METHOD(captureSelfie:(NSDictionary *)MTConfiguration resolve:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject){
  dispatch_async(dispatch_get_main_queue(), ^{
    MiSnapSwiftVC *picker = [[MiSnapSwiftVC alloc] init];
    [picker snapItButtonPressed:@"selfie"];
    picker.delegate = self;
    self->_callbackSucessBlock = resolve;
    self->_callbackFailureBlock = reject;
    UINavigationController* pickerNavigator = [[UINavigationController alloc] initWithRootViewController:picker];
    pickerNavigator.modalPresentationStyle = UIModalPresentationFullScreen;
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [delegate goNativeViewController:pickerNavigator];
  });
}

//promise - callback

- (void)MiSnapVCDelegateFinishedReturningEncodedImage:(nonnull NSString *)encodedImage results:(nonnull NSDictionary *)results andOriginalImage:(nonnull UIImage *)image {
  NSString *jsonString = results[@"MiSnapMIBIData"] ;
  if(!jsonString.length) {
    _callbackSucessBlock(@{@"encodedImage":encodedImage ,@"results": results, @"originalImage": image});
  } else {
    NSData *data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSString *documentType = [json objectForKey:@"Document"];
    
    if ([documentType isEqualToString:@"ID_CARD_BACK"]) {
      _callbackSucessBlock(@{@"results": results, @"PDF417Code": results[@"MiSnapPDF417Data"]});
    } else {
      _callbackSucessBlock(@{@"encodedImage":encodedImage ,@"results": results, @"originalImage": image});
    }
  }
}

- (void)MiSnapVCDelegateCancelledType:(nonnull NSString *)type message:(nonnull NSString *)message {
  _callbackFailureBlock(type, message, nil);
}


@end
