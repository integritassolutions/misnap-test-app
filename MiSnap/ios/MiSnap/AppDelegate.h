#import <React/RCTBridgeDelegate.h>
#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, RCTBridgeDelegate>

@property (nonatomic, strong) UIWindow *window;
// keep a reference to the React Native VC
@property (nonatomic, strong) UIViewController *reactNativeViewController;
- (void)goNativeViewController:(UIViewController *)vc;
- (void)goToReactNative;

@end
